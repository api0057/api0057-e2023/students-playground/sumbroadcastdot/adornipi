from extension_helpers import get_extensions
from setuptools import setup

setup(
    ext_modules=get_extensions(),
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
)
