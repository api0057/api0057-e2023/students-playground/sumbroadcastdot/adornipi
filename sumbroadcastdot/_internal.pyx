
#cython: language_level=3, boundscheck=False, wraparound=False, initializedcheck=False, cdivision=True

def sp4(double[:,:] Aij, double[:,:] Bik, double[:,:] Cil, double[:,:] Djk,
        double[:,:] Ejl, double[:,:] Fkl):
    cdef double ret = 0
    cdef double parret1, parret2
    cdef Py_ssize_t i,j,k,l
    cdef Py_ssize_t n = Aij.shape[0]
    for i in range(n):
        for j in range(n):
            parret2 = 0
            for k in range(n):
                parret1 = 0
                for l in range(n):
                    parret1 += Cil[i,l]*Ejl[j,l]*Fkl[k,l]
                parret2 += Bik[i,k]*Djk[j,k]*parret1
            ret += Aij[i,j]*parret2
    return ret
